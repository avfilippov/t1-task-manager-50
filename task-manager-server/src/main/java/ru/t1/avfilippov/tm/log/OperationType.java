package ru.t1.avfilippov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
