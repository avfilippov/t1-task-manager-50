package ru.t1.avfilippov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}
