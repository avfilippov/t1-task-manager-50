package ru.t1.avfilippov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.dto.IUserOwnedRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedDTOService <M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    public AbstractUserOwnedDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final EntityManager entityManager);


    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model.setUserId(userId);
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public  List<M> findAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public  List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public  M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public  List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.findAll(userId, sort.getComparator());
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = findOneById(userId, id);
        if (model != null) remove(userId, model);
    }

}
