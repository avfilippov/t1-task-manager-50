package ru.t1.avfilippov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import liquibase.pro.packaged.S;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.service.IAdminService;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.exception.user.AccessDeniedException;

public class AdminService implements IAdminService {

    private final IConnectionService connectionService;

    private final IPropertyService propertyService;

    public AdminService(
            final IConnectionService connectionService,
            final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void dropScheme(@Nullable final String initToken) {
        @NotNull final String token = propertyService.getInitToken();
        if (initToken.isEmpty() || !token.equals(initToken)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
        liquibase.close();
    }

    @Override
    @SneakyThrows
    public void initScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getInitToken();
        if (initToken.isEmpty() || !token.equals(initToken)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
        liquibase.close();
    }

}
