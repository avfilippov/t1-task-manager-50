package ru.t1.avfilippov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.component.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
