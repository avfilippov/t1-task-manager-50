package ru.t1.avfilippov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class EndpointException extends AbstractException {
    public EndpointException(@NotNull final String message) {
        super(message);
    }

}
